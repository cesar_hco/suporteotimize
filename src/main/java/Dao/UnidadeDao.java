package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Conexao.Conexao;
import Modelo.UnidadeOrganizacional;

public class UnidadeDao {

public ArrayList<UnidadeOrganizacional> unidadeOrganizacional() throws SQLException {		
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT codigoCliente,nomeCliente FROM unidadeOrganizacional ");
		
		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<UnidadeOrganizacional> listaUnidadeOrganizacional = new ArrayList<UnidadeOrganizacional>();

		while (resultado.next()) {
			
		
			UnidadeOrganizacional unidadeOrganizacional = new UnidadeOrganizacional();
			
			unidadeOrganizacional.setCodigoCliente(resultado.getInt("codigoCliente"));
			unidadeOrganizacional.setNomeCliente(resultado.getString("nomeCliente"));
											
			
			listaUnidadeOrganizacional.add(unidadeOrganizacional);			
			
			
		}
		return listaUnidadeOrganizacional;
	}
	
}

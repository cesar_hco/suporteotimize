package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Conexao.Conexao;
import Modelo.HistoricoProtocolo;
import Modelo.Protocolo;

public class HistoricoProtocoloDao {

	Connection conexao;

	public HistoricoProtocoloDao() {
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}
	
	
	public void VerificarExistenciaProtocolo(String clienteProtocolo , String responsavelProtocolo , HistoricoProtocolo historicoProtocolo) throws SQLException {
		
        StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT codigoProcolo , situacaoProtocolo , clienteProtocolo , responsavelProtocolo FROM protocolo where situacaoProtocolo = 'ABERTO' and clienteProtocolo = '").append(clienteProtocolo).append("' And responsavelProtocolo = '").append(responsavelProtocolo).append("'");
		
		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();		

		while (resultado.next()) {
		
			Protocolo protocolo = new Protocolo();
			
			protocolo.setCodigoProcolo(resultado.getInt("codigoProcolo"));
			protocolo.setSituacaoProtocolo(resultado.getString("situacaoProtocolo"));
			protocolo.setClienteProtocolo(resultado.getString("clienteProtocolo"));
			protocolo.setResponsavelProtocolo(resultado.getString("responsavelProtocolo"));
			
			salvarHistoricoProtocolo(protocolo , historicoProtocolo);
			
		}
		conexao.close();
	}
	
	
	
	public void salvarHistoricoProtocolo(Protocolo protocolo ,  HistoricoProtocolo historicoProtocolo) throws SQLException {
		
	StringBuilder sql = new StringBuilder();
	
	sql.append("insert into historicoprotocolo(mensagemenviadaprotocolo , mensagemrecebidaprotocolo,protocolohistorico,dataenviomensagem,datarecebidamensagem,responsavelmensagem) values (?,? ,?, ?, ?,?)");
	
	PreparedStatement gravarHistorico = conexao.prepareStatement(sql.toString());
	
	gravarHistorico.setString(1, historicoProtocolo.getMensagemEnviadaProtocolo());
	gravarHistorico.setString(2, historicoProtocolo.getMensagemRecebidaProtocolo());
	gravarHistorico.setInt(3, protocolo.getCodigoProcolo());
	gravarHistorico.setString(4, historicoProtocolo.getDataEnvioMensagem());
	gravarHistorico.setString(5, historicoProtocolo.getDataRecebidaMensagem());
	gravarHistorico.setString(6, historicoProtocolo.getResponsavelMensagem());
	
	gravarHistorico.executeUpdate();
	
	conexao.close();
	}
	
public void VerificarExistenciaProtocoloGrupo(String clienteProtocolo , String responsavelProtocolo , HistoricoProtocolo historicoProtocolo) throws SQLException {
		
        StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT codigoProcolo , situacaoProtocolo , clienteProtocolo , responsavelProtocolo FROM protocolo where situacaoProtocolo = 'ABERTO' and clienteProtocolo = '").append(clienteProtocolo).append("'");
		
		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();		

		while (resultado.next()) {
		
			Protocolo protocolo = new Protocolo();
			
			protocolo.setCodigoProcolo(resultado.getInt("codigoProcolo"));
			protocolo.setSituacaoProtocolo(resultado.getString("situacaoProtocolo"));
			protocolo.setClienteProtocolo(resultado.getString("clienteProtocolo"));
			protocolo.setResponsavelProtocolo(resultado.getString("responsavelProtocolo"));
			
			salvarHistoricoProtocoloGrupo(protocolo , historicoProtocolo);
			
		}
		conexao.close();
	}
	
	
	public void salvarHistoricoProtocoloGrupo(Protocolo protocolo ,  HistoricoProtocolo historicoProtocolo) throws SQLException {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("insert into historicoprotocolo(mensagemenviadaprotocolo , mensagemrecebidaprotocolo,protocolohistorico,dataenviomensagem,datarecebidamensagem,responsavelmensagem) values (?,? ,?, ?, ?,?)");
		
		PreparedStatement gravarHistorico = conexao.prepareStatement(sql.toString());
		
		gravarHistorico.setString(1, historicoProtocolo.getMensagemEnviadaProtocolo());
		gravarHistorico.setString(2, historicoProtocolo.getMensagemRecebidaProtocolo());
		gravarHistorico.setInt(3, protocolo.getCodigoProcolo());
		gravarHistorico.setString(4, historicoProtocolo.getDataEnvioMensagem());
		gravarHistorico.setString(5, historicoProtocolo.getDataRecebidaMensagem());
		gravarHistorico.setString(6, historicoProtocolo.getResponsavelMensagem());
		
		gravarHistorico.executeUpdate();
		
		conexao.close();
		}	
	
		
		
		public ArrayList<HistoricoProtocolo> consultarHistorico(String Protocolo) throws SQLException {

			StringBuilder sql = new StringBuilder();

			
			sql.append("select codigohistoricoprotocolo,codigoProcolo,numeroProtocolo ,mensagemenviadaprotocolo , mensagemrecebidaprotocolo , protocolohistorico , datarecebidamensagem from historicoprotocolo ");
			sql.append(" inner join Protocolo on historicoprotocolo.protocolohistorico = Protocolo.codigoProcolo ");
			sql.append(" where numeroProtocolo  = '").append(Protocolo).append("' order by 1 ");
			
			
			Connection conexao = Conexao.getConexao();
			PreparedStatement comando = conexao.prepareStatement(sql.toString());

			ResultSet resultado = comando.executeQuery();

			ArrayList<HistoricoProtocolo> listaHistorico = new ArrayList<HistoricoProtocolo>();

			while (resultado.next()) {
				
						

				Protocolo protocolo = new Protocolo();

				protocolo.setCodigoProcolo(resultado.getInt("codigoProcolo"));
				protocolo.setNumeroProtocolo(resultado.getString("numeroProtocolo"));
				
				HistoricoProtocolo historicoProtocolo = new HistoricoProtocolo();
				
				historicoProtocolo.setCodigoHistoricoProtocolo(resultado.getInt("codigohistoricoprotocolo"));
				historicoProtocolo.setMensagemEnviadaProtocolo(resultado.getString("mensagemenviadaprotocolo"));
				historicoProtocolo.setMensagemRecebidaProtocolo(resultado.getString("mensagemrecebidaprotocolo"));
				historicoProtocolo.setDataRecebidaMensagem(resultado.getString("datarecebidamensagem"));	
				
				

				listaHistorico.add(historicoProtocolo);
			}
			conexao.close();
			return listaHistorico;
		}
		
	}
	
	

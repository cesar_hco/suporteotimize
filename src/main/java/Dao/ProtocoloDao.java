package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import Conexao.Conexao;
import Modelo.FuncionarioOtimize;
import Modelo.HistoricoProtocolo;
import Modelo.Protocolo;

public class ProtocoloDao {

	Connection conexao;
	private final Lock bloqueio = new ReentrantLock();

	public ProtocoloDao() {
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}

	public boolean semProtocoloFinalizar = true;

	public boolean aberturaProtocolo = false;

	public boolean isSemProtocoloFinalizar() {
		return semProtocoloFinalizar;
	}

	public void setSemProtocoloFinalizar(boolean semProtocoloFinalizar) {
		this.semProtocoloFinalizar = semProtocoloFinalizar;
	}

	public boolean isAberturaProtocolo() {
		return aberturaProtocolo;
	}

	public void setAberturaProtocolo(boolean aberturaProtocolo) {
		this.aberturaProtocolo = aberturaProtocolo;
	}

	public void GerarProtocolo(Protocolo protocolo, HistoricoProtocolo historicoProtocolo) throws SQLException {

		StringBuilder sql = new StringBuilder();
		StringBuilder buscarNumero = new StringBuilder();

		sql.append(
				"INSERT INTO protocolo(numeroProtocolo, clienteProtocolo, responsavelProtocolo, aberturaProtocolo,situacaoProtocolo) VALUES( ? , ? , ? , ? , ?)");

		buscarNumero.append("select max(codigoProcolo)+1 as codigoProcolo from protocolo  ");

		Connection conexao = Conexao.getConexao();
		PreparedStatement buscar = conexao.prepareStatement(buscarNumero.toString());		

		ResultSet resultado = buscar.executeQuery();

		ArrayList<Protocolo> listaProtocoloAberto = new ArrayList<Protocolo>();

		while (resultado.next()) {
			Protocolo numeroGerado = new Protocolo();

			numeroGerado.setCodigoProcolo(resultado.getInt("codigoProcolo"));

			PreparedStatement gerarProtocolo = conexao.prepareStatement(sql.toString());

			gerarProtocolo.setString(1,
					protocolo.getNumeroProtocolo().concat(Integer.toString(numeroGerado.getCodigoProcolo())));
			gerarProtocolo.setString(2, protocolo.getClienteProtocolo());
			gerarProtocolo.setString(3, protocolo.getResponsavelProtocolo());
			gerarProtocolo.setString(4, protocolo.getAberturaProtocolo());
			gerarProtocolo.setString(5, protocolo.getSituacaoProtocolo());

			gerarProtocolo.executeUpdate();

			aberturaProtocolo = true;

			HistoricoProtocoloDao historicoProtocoloDao = new HistoricoProtocoloDao();

			historicoProtocoloDao.VerificarExistenciaProtocolo(protocolo.getClienteProtocolo(),
					protocolo.getResponsavelProtocolo(), historicoProtocolo);
		}
		conexao.close();
	}

	public void ValidarExistenciaProtocolo(Protocolo Verificarprotocolo, boolean finalizar, boolean mensagemEnviada,
			HistoricoProtocolo historicoProtocolo) throws SQLException {

		boolean novoProtocolo = true;

		StringBuilder sql = new StringBuilder();

		sql.append(
				"SELECT codigoProcolo , situacaoProtocolo , clienteProtocolo , responsavelProtocolo , numeroProtocolo FROM protocolo where situacaoProtocolo = 'ABERTO' and clienteProtocolo = '")
				.append(Verificarprotocolo.getClienteProtocolo()).append("' And responsavelProtocolo = '")
				.append(Verificarprotocolo.getResponsavelProtocolo()).append("'");

		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();
		 

		ArrayList<Protocolo> listaProtocoloAberto = new ArrayList<Protocolo>();

		while (resultado.next()) {

			novoProtocolo = false;

			Protocolo protocolo = new Protocolo();

			protocolo.setCodigoProcolo(resultado.getInt("codigoProcolo"));
			protocolo.setSituacaoProtocolo(resultado.getString("situacaoProtocolo"));
			protocolo.setClienteProtocolo(resultado.getString("clienteProtocolo"));
			protocolo.setResponsavelProtocolo(resultado.getString("responsavelProtocolo"));
			protocolo.setNumeroProtocolo(resultado.getString("numeroProtocolo"));

			protocolo.setFechamentoProtocolo(Verificarprotocolo.getFechamentoProtocolo());

			HistoricoProtocoloDao historicoProtocoloDao = new HistoricoProtocoloDao();

			historicoProtocoloDao.VerificarExistenciaProtocolo(protocolo.getClienteProtocolo(),
					protocolo.getResponsavelProtocolo(), historicoProtocolo);

			if (finalizar == true) {
				FinalizarProtocolo(protocolo);
				NumeroProtocoloFinalizado(protocolo.getNumeroProtocolo());
			}
			if (finalizar == false) {

			}
			conexao.close();
		}

		if (novoProtocolo == true && mensagemEnviada == false) {
			GerarProtocolo(Verificarprotocolo, historicoProtocolo);
			novoProtocolo = false;
		}
	}

	String numeroFinalizadoProtocolo;

	public String getNumeroFinalizadoProtocolo() {
		return numeroFinalizadoProtocolo;
	}

	public void setNumeroFinalizadoProtocolo(String numeroFinalizadoProtocolo) {
		this.numeroFinalizadoProtocolo = numeroFinalizadoProtocolo;
	}

	public void NumeroProtocoloFinalizado(String numeroFinalizado) {
		setNumeroFinalizadoProtocolo(numeroFinalizado);
	}

	public void FinalizarProtocolo(Protocolo protocolo) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("UPDATE protocolo SET ");
		sql.append("situacaoProtocolo = 'FINALIZADO' ,");
		sql.append("fechamentoProtocolo = ? ");
		sql.append("WHERE codigoProcolo = ? ");

		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		comando.setString(1, protocolo.getFechamentoProtocolo());
		comando.setInt(2, protocolo.getCodigoProcolo());

		comando.executeUpdate();
		semProtocoloFinalizar();
	}

	public void semProtocoloFinalizar() {
		semProtocoloFinalizar = false;
	}

	public ArrayList<Protocolo> ListarProtocoloAberto() throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM protocolo where situacaoProtocolo = 'ABERTO' ");

		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<Protocolo> listaProtocoloAberto = new ArrayList<Protocolo>();

		while (resultado.next()) {

			Protocolo protocolo = new Protocolo();

			protocolo.setCodigoProcolo(resultado.getInt("codigoProcolo"));
			protocolo.setNumeroProtocolo(resultado.getString("numeroProtocolo"));
			protocolo.setSituacaoProtocolo(resultado.getString("situacaoProtocolo"));
			protocolo.setClienteProtocolo(resultado.getString("clienteProtocolo"));
			protocolo.setResponsavelProtocolo(resultado.getString("responsavelProtocolo"));
			protocolo.setAberturaProtocolo(resultado.getString("aberturaProtocolo"));

			listaProtocoloAberto.add(protocolo);
		}
		return listaProtocoloAberto;
	}

	public ArrayList<Protocolo> ListarProtocoloFechado() throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM protocolo where situacaoProtocolo = 'FINALIZADO' ");

		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<Protocolo> listaProtocoloFechado = new ArrayList<Protocolo>();

		while (resultado.next()) {

			Protocolo protocolo = new Protocolo();

			protocolo.setCodigoProcolo(resultado.getInt("codigoProcolo"));
			protocolo.setNumeroProtocolo(resultado.getString("numeroProtocolo"));
			protocolo.setSituacaoProtocolo(resultado.getString("situacaoProtocolo"));
			protocolo.setClienteProtocolo(resultado.getString("clienteProtocolo"));
			protocolo.setResponsavelProtocolo(resultado.getString("responsavelProtocolo"));
			protocolo.setAberturaProtocolo(resultado.getString("aberturaProtocolo"));
			protocolo.setFechamentoProtocolo(resultado.getString("fechamentoProtocolo"));

			listaProtocoloFechado.add(protocolo);
		}
		return listaProtocoloFechado;
	}

	public synchronized void ValidarExistenciaProtocoloGrupo(Protocolo Verificarprotocolo, boolean finalizar,
			boolean mensagemEnviada, HistoricoProtocolo historicoProtocolo) throws SQLException {

		boolean novoProtocolo = true;
		boolean historicoNovo = true;

		StringBuilder sql = new StringBuilder();
		bloqueio.lock();
		try {
			sql.append(
					"select codigoProcolo , situacaoProtocolo , clienteProtocolo ,responsavelProtocolo,numeroProtocolo , HP.mensagemRecebidaProtocolo, HP.mensagemenviadaprotocolo from historicoprotocolo HP\r\n"
							+ " inner join protocolo P on HP.protocoloHistorico = P.codigoprocolo\r\n"
							+ "where situacaoProtocolo = 'ABERTO' and clienteProtocolo = '")
					.append(Verificarprotocolo.getClienteProtocolo()).append("'");

			Connection conexao = Conexao.getConexao();
			PreparedStatement comando = conexao.prepareStatement(sql.toString());

			ResultSet resultado = comando.executeQuery();

			ArrayList<Protocolo> listaProtocoloAberto = new ArrayList<Protocolo>();

			while (resultado.next()) {

				novoProtocolo = false;

				Protocolo protocolo = new Protocolo();

				protocolo.setCodigoProcolo(resultado.getInt("codigoProcolo"));
				protocolo.setSituacaoProtocolo(resultado.getString("situacaoProtocolo"));
				protocolo.setClienteProtocolo(resultado.getString("clienteProtocolo"));
				protocolo.setResponsavelProtocolo(resultado.getString("responsavelProtocolo"));
				protocolo.setNumeroProtocolo(resultado.getString("numeroProtocolo"));

				HistoricoProtocolo historico = new HistoricoProtocolo();

				historico.setMensagemRecebidaProtocolo(resultado.getString("mensagemRecebidaProtocolo"));
				historico.setMensagemEnviadaProtocolo(resultado.getString("mensagemenviadaprotocolo"));

				protocolo.setFechamentoProtocolo(Verificarprotocolo.getFechamentoProtocolo());

				if (historico.getMensagemRecebidaProtocolo()
						.equals(historicoProtocolo.getMensagemRecebidaProtocolo())) {
					historicoNovo = false;
				}

				if (protocolo.getResponsavelProtocolo() == null) {

					if (historicoProtocolo.getMensagemRecebidaProtocolo()
							.equals(historico.getMensagemRecebidaProtocolo())) {

					} else {

						int codigo = protocolo.getCodigoProcolo();

						if (historicoProtocolo.getMensagemEnviadaProtocolo() == null) {

						} else {

							String responsavel = historicoProtocolo.getMensagemEnviadaProtocolo().replace("8:", "");

							VerificarExistenciaNoGrupo(responsavel, codigo);
						}
					}

				}
				if (finalizar == true) {
					FinalizarProtocolo(protocolo);
					NumeroProtocoloFinalizado(protocolo.getNumeroProtocolo());
				}
				if (finalizar == false) {

				}
			}

			if (novoProtocolo == true && mensagemEnviada == false) {
				if (historicoProtocolo.getMensagemRecebidaProtocolo().equals("--")) {

				} else {
					GerarProtocoloGrupo(Verificarprotocolo, historicoProtocolo);
					novoProtocolo = false;
					historicoNovo = false;
				}
			}

			if (historicoNovo == true) {

				HistoricoProtocoloDao historicoProtocoloDao = new HistoricoProtocoloDao();

				historicoProtocoloDao.VerificarExistenciaProtocoloGrupo(Verificarprotocolo.getClienteProtocolo(),
						Verificarprotocolo.getResponsavelProtocolo(), historicoProtocolo);
			}
		} finally {
			bloqueio.unlock();
		}
	}

	public void GerarProtocoloGrupo(Protocolo protocolo, HistoricoProtocolo historicoProtocolo) throws SQLException {

		StringBuilder sql = new StringBuilder();
		StringBuilder buscarNumero = new StringBuilder();

		sql.append(
				"INSERT INTO protocolo(numeroProtocolo, clienteProtocolo,  aberturaProtocolo,situacaoProtocolo) VALUES( ? ,  ? , ? , ?)");

		buscarNumero.append("select max(codigoProcolo)+1 as codigoProcolo from protocolo  ");

		Connection conexao = Conexao.getConexao();
		PreparedStatement buscar = conexao.prepareStatement(buscarNumero.toString());

		ResultSet resultado = buscar.executeQuery();

		ArrayList<Protocolo> listaProtocoloAberto = new ArrayList<Protocolo>();

		while (resultado.next()) {
			Protocolo numeroGerado = new Protocolo();

			numeroGerado.setCodigoProcolo(resultado.getInt("codigoProcolo"));

			PreparedStatement gerarProtocolo = conexao.prepareStatement(sql.toString());

			gerarProtocolo.setString(1,
					protocolo.getNumeroProtocolo().concat(Integer.toString(numeroGerado.getCodigoProcolo())));
			gerarProtocolo.setString(2, protocolo.getClienteProtocolo());

			gerarProtocolo.setString(3, protocolo.getAberturaProtocolo());
			gerarProtocolo.setString(4, protocolo.getSituacaoProtocolo());

			gerarProtocolo.executeUpdate();

			aberturaProtocolo = true;

			HistoricoProtocoloDao historicoProtocoloDao = new HistoricoProtocoloDao();

			historicoProtocoloDao.VerificarExistenciaProtocoloGrupo(protocolo.getClienteProtocolo(),
					protocolo.getResponsavelProtocolo(), historicoProtocolo);
		}

	}

	public void IncrementarResponsavel(String responsavelProtocolo, int codigoProtocolo) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("UPDATE protocolo SET ");
		sql.append("responsavelProtocolo = ? ");
		sql.append("WHERE codigoProcolo = ? ");

		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		comando.setString(1, responsavelProtocolo);
		comando.setInt(2, codigoProtocolo);

		comando.executeUpdate();

	}

	public void VerificarExistenciaNoGrupo(String ResponsavelMensagem, int codigoProtocolo) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT usuarioSkype , utilizarSkype FROM funcionariootimize where  usuarioSkype = '")
				.append(ResponsavelMensagem).append("'");

		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<Protocolo> listaProtocoloAberto = new ArrayList<Protocolo>();

		while (resultado.next()) {

			FuncionarioOtimize funcionarioOtimize = new FuncionarioOtimize();

			funcionarioOtimize.setUsuarioSkype(resultado.getString("usuarioSkype"));

			funcionarioOtimize.setUtilizarSkype(resultado.getBoolean("utilizarSkype"));

			IncrementarResponsavel(funcionarioOtimize.getUsuarioSkype(), codigoProtocolo);

		}
	}

	public ArrayList<Protocolo> ListarProtocolos(String valorInformado,  String dataincio, String datafim,
			String tipoconsulta, String situacao) throws SQLException {

		StringBuilder sql = new StringBuilder();

		if (tipoconsulta.equals("protocolo")) {

			/*sql.append("SELECT * FROM protocolo where numeroProtocolo ilike '").append(valorInformado).append("'");*/
			sql.append("select * from protocolo  inner join funcionariootimize  on protocolo.responsavelProtocolo = funcionariootimize.usuarioSkype where protocolo.numeroProtocolo ilike '%").append(valorInformado).append("'");



		}
		if (tipoconsulta.equals("cliente") && !situacao.equals("AMBOS")) {

			/*sql.append("SELECT * FROM protocolo where clienteprotocolo ilike '%").append(valorInformado);*/
			sql.append("select * from protocolo  inner join funcionariootimize  on protocolo.responsavelProtocolo = funcionariootimize.usuarioSkype where protocolo.clienteprotocolo ilike '%").append(valorInformado);

			
			if (dataincio.equals("")) {
				sql.append("%' and situacaoProtocolo = '").append(situacao).append("'");
			}else {
			sql.append("%'  and aberturaProtocolo::DATE between to_date( '").append(dataincio).append("', 'dd/mm/yyyy')");
			sql.append("and to_date( '").append(datafim).append("', 'dd/mm/yyyy')  and situacaoProtocolo = '").append(situacao).append("'");
			}
		}if (tipoconsulta.equals("cliente") && situacao.equals("AMBOS")) {
			
			sql.append("select * from protocolo  inner join funcionariootimize  on protocolo.responsavelProtocolo = funcionariootimize.usuarioSkype where protocolo.clienteprotocolo ilike '%").append(valorInformado);


			if (dataincio.equals("")) {
				sql.append("%' and situacaoProtocolo = '").append(situacao).append("'");
			}else {
			sql.append("%'  and aberturaProtocolo::DATE between to_date( '").append(dataincio).append("', 'dd/mm/yyyy')");
			sql.append("and to_date( '").append(datafim).append("', 'dd/mm/yyyy')");
			}
		}
		
		if (tipoconsulta.equals("responsavel") && !situacao.equals("AMBOS")) {

			sql.append("select * from protocolo  inner join funcionariootimize  on protocolo.responsavelProtocolo = funcionariootimize.usuarioSkype where funcionariootimize.nomeFuncionario ilike '%").append(valorInformado);

			if (dataincio.equals("")) {
				sql.append("%' and situacaoProtocolo = '").append(situacao).append("'");
			}else {
			sql.append("%'  and aberturaProtocolo::DATE between to_date( '").append(dataincio).append("', 'dd/mm/yyyy')");
			sql.append("and to_date( '").append(datafim).append("', 'dd/mm/yyyy')  and situacaoProtocolo = '").append(situacao).append("'");
			}
		}if (tipoconsulta.equals("responsavel") && situacao.equals("AMBOS")) {
			
			sql.append("select * from protocolo  inner join funcionariootimize  on protocolo.responsavelProtocolo = funcionariootimize.usuarioSkype where funcionariootimize.nomeFuncionario ilike '%").append(valorInformado);

			if (dataincio.equals("")) {
				sql.append("%'");
			}else {
			sql.append("%'  and aberturaProtocolo::DATE between to_date( '").append(dataincio).append("', 'dd/mm/yyyy')");
			sql.append("and to_date( '").append(datafim).append("', 'dd/mm/yyyy')");
			}
		}
		
		
		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<Protocolo> listaProtocolo = new ArrayList<Protocolo>();

		while (resultado.next()) {
			
			FuncionarioOtimize funcionarioOtimize = new FuncionarioOtimize();
			
			funcionarioOtimize.setCodigoFuncionario(resultado.getInt("codigoFuncionario"));
			funcionarioOtimize.setNomeFuncionario(resultado.getString("nomeFuncionario"));
			funcionarioOtimize.setUsuarioSkype(resultado.getString("usuarioSkype"));
			

			Protocolo protocolo = new Protocolo();

			protocolo.setCodigoProcolo(resultado.getInt("codigoProcolo"));
			protocolo.setNumeroProtocolo(resultado.getString("numeroProtocolo"));
			protocolo.setSituacaoProtocolo(resultado.getString("situacaoProtocolo"));
			protocolo.setClienteProtocolo(resultado.getString("clienteProtocolo"));
			protocolo.setResponsavelProtocolo(resultado.getString("responsavelProtocolo"));
			protocolo.setAberturaProtocolo(resultado.getString("aberturaProtocolo"));
			protocolo.setFechamentoProtocolo(resultado.getString("fechamentoProtocolo"));

			listaProtocolo.add(protocolo);
		}
		return listaProtocolo;
	}

}

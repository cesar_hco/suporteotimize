package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Bean.Logica;
import Conexao.Conexao;
import Modelo.FuncionarioOtimize;

public class FuncionarioOtimizeDao {
	
      

public ArrayList<FuncionarioOtimize> funcionarioOtimize() throws SQLException {		
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT codigoFuncionario,nomeFuncionario,usuarioSkype , senhaSkype , utilizarSkype  FROM funcionarioOtimize where utilizarSkype = 'true' ");
		
		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();

		ArrayList<FuncionarioOtimize> listaFuncionarioOtimize = new ArrayList<FuncionarioOtimize>();

		while (resultado.next()) {
			
		
			FuncionarioOtimize funcionarioOtimize = new FuncionarioOtimize();
			
			funcionarioOtimize.setCodigoFuncionario(resultado.getInt("codigoFuncionario"));
			funcionarioOtimize.setNomeFuncionario(resultado.getString("nomeFuncionario"));
			funcionarioOtimize.setUsuarioSkype(resultado.getString("usuarioSkype"));
			funcionarioOtimize.setSenhaSkype(resultado.getString("senhaSkype"));
			funcionarioOtimize.setUtilizarSkype(resultado.getBoolean("utilizarSkype"));								
			
			listaFuncionarioOtimize.add(funcionarioOtimize);			
			
			
		}
		return listaFuncionarioOtimize;
	}
public ArrayList<FuncionarioOtimize> funcionarioPresenteOtimize() throws SQLException {		
	
	StringBuilder sql = new StringBuilder();
	
	sql.append("SELECT codigoFuncionario, usuarioSkype , utilizarSkype , presenteEmpresa  FROM funcionarioOtimize where presenteEmpresa = 'true' ");
	
	Connection conexao = Conexao.getConexao();
	PreparedStatement comando = conexao.prepareStatement(sql.toString());

	ResultSet resultado = comando.executeQuery();

	ArrayList<FuncionarioOtimize> listaFuncionarioOtimize = new ArrayList<FuncionarioOtimize>();

	while (resultado.next()) {
		
	
		FuncionarioOtimize funcionarioOtimize = new FuncionarioOtimize();
		
		funcionarioOtimize.setCodigoFuncionario(resultado.getInt("codigoFuncionario"));		
		funcionarioOtimize.setSenhaSkype(resultado.getString("senhaSkype"));
		funcionarioOtimize.setUtilizarSkype(resultado.getBoolean("utilizarSkype"));
		funcionarioOtimize.setPresenteEmpresa(resultado.getBoolean("presenteEmpresa"));	
		
		listaFuncionarioOtimize.add(funcionarioOtimize);			
		
		
	}
	return listaFuncionarioOtimize;
}
}
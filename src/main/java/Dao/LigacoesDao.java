package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Conexao.Conexao;
import Modelo.FuncionarioOtimize;
import Modelo.Ligacoes;
import Modelo.Protocolo;
import Modelo.UnidadeOrganizacional;

public class LigacoesDao {

	Connection conexao;

	public LigacoesDao() {
		try {
			conexao = Conexao.getConexao();
		} catch (SQLException e) {
			System.out.println("Erro ao conectar com banco de dados" + e.getMessage());
		}
	}

	public void GravarLigacoes(Ligacoes ligacoes) throws SQLException {

		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO ligacoes(unidadeOrganizacional ,dataLigacao ,funcionarioUnidade ,responsavelOtimize ,observacaoLigacao) ");
		sql.append("VALUES(?,?,?,?,?)");

		PreparedStatement gravarLigacoes = conexao.prepareStatement(sql.toString());

		gravarLigacoes.setInt(1, ligacoes.getUnidadeOrganizacional().getCodigoCliente());
		gravarLigacoes.setDate(2, ligacoes.getDataLigacao());
		gravarLigacoes.setString(3, ligacoes.getFuncionarioUnidade());
		gravarLigacoes.setInt(4, ligacoes.getResponsavelOtimize().getCodigoFuncionario());
		gravarLigacoes.setString(5, ligacoes.getObservacaoLigacao());
		
		gravarLigacoes.executeUpdate();

	}
	
	public  ArrayList<Ligacoes> ConsultarLigacoes() throws SQLException {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT funcionariootimize.nomeFuncionario , unidadeOrganizacional.nomeCliente , ligacoes.codigoLigacoes , ligacoes.dataLigacao,ligacoes.funcionarioUnidade , ligacoes.observacaoLigacao FROM ligacoes");
		sql.append(" INNER JOIN funcionariootimize on ligacoes.responsavelOtimize = funcionariootimize.codigoFuncionario ");
		sql.append("INNER JOIN unidadeorganizacional on ligacoes.unidadeorganizacional = unidadeorganizacional.codigocliente");
		
		Connection conexao = Conexao.getConexao();
		PreparedStatement comando = conexao.prepareStatement(sql.toString());

		ResultSet resultado = comando.executeQuery();
		
		ArrayList<Ligacoes> listaLigacoes = new ArrayList<Ligacoes>();

		while (resultado.next()) {
			
			FuncionarioOtimize funcionarioOtimize = new FuncionarioOtimize();
			
			funcionarioOtimize.setNomeFuncionario(resultado.getString("nomeFuncionario"));
			
			UnidadeOrganizacional unidadeOrganizacional = new UnidadeOrganizacional();
			
			unidadeOrganizacional.setNomeCliente(resultado.getString("nomecliente"));			
			
			Ligacoes ligacoes = new Ligacoes();
			
			ligacoes.setCodigoLigacoes(resultado.getInt("codigoLigacoes"));
			ligacoes.setDataLigacao(resultado.getDate("dataLigacao"));
			ligacoes.setFuncionarioUnidade(resultado.getString("funcionarioUnidade"));
			ligacoes.setResponsavelOtimize(funcionarioOtimize);
			ligacoes.setUnidadeOrganizacional(unidadeOrganizacional);
			ligacoes.setObservacaoLigacao(resultado.getString("observacaoLigacao"));
			
			listaLigacoes.add(ligacoes);
			
		}
		return listaLigacoes;
	}

}

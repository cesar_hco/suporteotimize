import java.util.Objects;

public final class DataUTC implements Comparable<DataUTC> {
    private final int segundo;
    private final int minuto;
    private final int hora;
    private final int dia;
    private final int mes;
    private final long ano;

    private static int restoSemSinal(long a, int b) {
        return (int) (a >= 0L
                ? a % b // Positivo.
                : (b + (a % b)) % b); // Negativo.
    }

    private static long divisaoSemSinal(long a, int b) {
        return a >= 0L
                ? a / b // Positivo.
                : (a / b) - (a % b == 0 ? 0 : 1); // Negativo.
    }

    public DataUTC(int segundo, int minuto, int hora, int dia, int mes, long ano) {
        this.segundo = segundo;
        this.minuto = minuto;
        this.hora = hora;
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    public DataUTC(long timestampUnix) {
        // Passo 1.
        long minutosUnix = divisaoSemSinal(timestampUnix, 60);
        segundo = restoSemSinal(timestampUnix, 60);

        // Passo 2.
        long horasUnix = divisaoSemSinal(minutosUnix, 60);
        minuto = restoSemSinal(minutosUnix, 60);

        // Passo 3.
        long diasUnix = divisaoSemSinal(horasUnix, 24);
        hora = restoSemSinal(horasUnix, 24);

        // Passo 4.
        long ciclosDe400Anos = divisaoSemSinal(diasUnix, 146097);
        int diasEm400Anos = restoSemSinal(diasUnix, 146097);

        // Passo 5.
        if (diasEm400Anos >= 32 * 1461 + 789) diasEm400Anos++;
        if (diasEm400Anos >= 57 * 1461 + 789) diasEm400Anos++;
        if (diasEm400Anos >= 82 * 1461 + 789) diasEm400Anos++;

        // Passo 6.
        int ciclosDe4Anos = diasEm400Anos / 1461;
        int diasEm4Anos = diasEm400Anos % 1461;

        // Passo 7.
        if (diasEm4Anos >= 59) diasEm4Anos++;
        if (diasEm4Anos >= 425) diasEm4Anos++;
        if (diasEm4Anos >= 1157) diasEm4Anos++;

        // Passo 8.
        int anoEm4Anos = diasEm4Anos / 366;
        int diasNoAno = diasEm4Anos % 366;

        // Passo 9.
        ano = anoEm4Anos + ciclosDe4Anos * 4 + ciclosDe400Anos * 400 + 1970;

        // Passo 10.
        int[] tabelaDeMeses = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int contagemDeMeses = 0;
        while (diasNoAno >= tabelaDeMeses[contagemDeMeses]) {
            diasNoAno -= tabelaDeMeses[contagemDeMeses];
            contagemDeMeses++;
        }
        mes = contagemDeMeses + 1;
        dia = diasNoAno + 1;
    }

    public long getTimestamp() {
        // Passo 1.
        long anosDesde1970 = ano - 1970;

        // Passo 2.
        long periodosDe400Anos = divisaoSemSinal(anosDesde1970, 400);
        int anoNoPeriodoDe400Anos = restoSemSinal(anosDesde1970, 400);

        // Passo 3.
        int periodosDe4AnosNos400 = anoNoPeriodoDe400Anos / 4;
        int anoNoPeriodoDe4Anos = anoNoPeriodoDe400Anos % 4;

        // Passo 4.
        int diasNosAnosAnterioresDoPeriodoDe4Anos = 365 * anoNoPeriodoDe4Anos + (anoNoPeriodoDe4Anos == 3 ? 1 : 0);

        // Passo 5.
        long diasNoAno = dia - 1;
        int[] tabelaDeMeses = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for (int i = 0; i < mes - 1; i++) {
            diasNoAno += tabelaDeMeses[i];
        }

        // Passo 6.
        long dias = diasNoAno
                + diasNosAnosAnterioresDoPeriodoDe4Anos
                + periodosDe4AnosNos400 * 1461
                + periodosDe400Anos * 146097;

        // Passo 7.
        if (anoNoPeriodoDe4Anos == 2 && mes > 2) dias++;
        if (anoNoPeriodoDe400Anos > 130 || (anoNoPeriodoDe400Anos == 130 && mes > 2)) dias--;
        if (anoNoPeriodoDe400Anos > 230 || (anoNoPeriodoDe400Anos == 230 && mes > 2)) dias--;
        if (anoNoPeriodoDe400Anos > 330 || (anoNoPeriodoDe400Anos == 330 && mes > 2)) dias--;

        // Passo 8.
        return segundo + 60 * minuto + 60 * 60 * hora + 60 * 60 * 24 * dias;
    }

    public static enum DiaDaSemana {
        DOMINGO, SEGUNDA_FEIRA, TERCA_FEIRA, QUARTA_FEIRA, QUINTA_FEIRA, SEXTA_FEIRA, SABADO;
    }

    public DiaDaSemana getDiaDaSemana() {
        // Passo 1.
        long diasDesde1970 = divisaoSemSinal(getTimestamp(), 86400);

        // Passo 2.
        int diaDaSemana = restoSemSinal(diasDesde1970 + 4, 7);

        // Passo 3.
        return DiaDaSemana.values()[diaDaSemana];
    }

    public int getSegundo() {
        return segundo;
    }

    public int getMinuto() {
        return minuto;
    }

    public int getHora() {
        return hora;
    }

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }

    public long getAno() {
        return ano;
    }

    @Override
    public int hashCode() {
        return Objects.hash(segundo, minuto, hora, dia, mes, ano);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DataUTC)) return false;
        DataUTC outro = (DataUTC) obj;
        return this.segundo == outro.segundo && this.minuto == outro.minuto && this.hora == outro.hora
                && this.dia == outro.dia && this.mes == outro.mes && this.ano == outro.ano;
    }

    @Override
    public int compareTo(DataUTC other) {
        Objects.requireNonNull(other);
        if (this.ano != other.ano) return this.ano > other.ano ? 1 : -1;
        if (this.mes != other.mes) return this.mes > other.mes ? 1 : -1;
        if (this.dia != other.dia) return this.dia > other.dia ? 1 : -1;
        if (this.hora != other.hora) return this.hora > other.hora ? 1 : -1;
        if (this.minuto != other.minuto) return this.minuto > other.minuto ? 1 : -1;
        if (this.segundo != other.segundo) return this.segundo > other.segundo ? 1 : -1;
        return 0;
    }

    @Override
    public String toString() {
        return String.format("%02d/%02d/%04d %02d:%02d:%02d", dia, mes, ano, hora-3, minuto, segundo);
    }
}
package Modelo;

public class UnidadeOrganizacional {

	private int codigoCliente;
	private String nomeCliente;
	
	public int getCodigoCliente() {
		return codigoCliente;
	}
	public void setCodigoCliente(int codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}	
	@Override
	public String toString() {
		return this.nomeCliente;
	}
	
}

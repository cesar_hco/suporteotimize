
package Modelo;

import java.sql.Date;

public class Ligacoes {

	private int codigoLigacoes;
	private UnidadeOrganizacional unidadeOrganizacional;
	private Date dataLigacao;
	private String funcionarioUnidade;
	private FuncionarioOtimize responsavelOtimize;	
	private String observacaoLigacao;
	
	public int getCodigoLigacoes() {
		return codigoLigacoes;
	}
	public void setCodigoLigacoes(int codigoLigacoes) {
		this.codigoLigacoes = codigoLigacoes;
	}
	public UnidadeOrganizacional getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}
	public Date getDataLigacao() {
		return dataLigacao;
	}
	public void setDataLigacao(Date dataLigacao) {
		this.dataLigacao = dataLigacao;
	}
	public String getFuncionarioUnidade() {
		return funcionarioUnidade;
	}
	public void setFuncionarioUnidade(String funcionarioUnidade) {
		this.funcionarioUnidade = funcionarioUnidade;
	}
	public FuncionarioOtimize getResponsavelOtimize() {
		return responsavelOtimize;
	}
	public void setResponsavelOtimize(FuncionarioOtimize responsavelOtimize) {
		this.responsavelOtimize = responsavelOtimize;
	}
	public String getObservacaoLigacao() {
		return observacaoLigacao;
	}
	public void setObservacaoLigacao(String observacaoLigacao) {
		this.observacaoLigacao = observacaoLigacao;
	}
	
	
	
	
	
}

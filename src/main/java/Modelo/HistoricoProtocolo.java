package Modelo;

public class HistoricoProtocolo {

	private int codigoHistoricoProtocolo;
	private String mensagemEnviadaProtocolo;
	private String mensagemRecebidaProtocolo;
	private Protocolo protocoloHistorico;
	private String dataEnvioMensagem;
	private String dataRecebidaMensagem;
	private String responsavelMensagem;

	public int getCodigoHistoricoProtocolo() {
		return codigoHistoricoProtocolo;
	}

	public void setCodigoHistoricoProtocolo(int codigoHistoricoProtocolo) {
		this.codigoHistoricoProtocolo = codigoHistoricoProtocolo;
	}

	public String getMensagemEnviadaProtocolo() {
		return mensagemEnviadaProtocolo;
	}

	public void setMensagemEnviadaProtocolo(String mensagemEnviadaProtocolo) {
		this.mensagemEnviadaProtocolo = mensagemEnviadaProtocolo;
	}

	public String getMensagemRecebidaProtocolo() {
		return mensagemRecebidaProtocolo;
	}

	public void setMensagemRecebidaProtocolo(String mensagemRecebidaProtocolo) {
		this.mensagemRecebidaProtocolo = mensagemRecebidaProtocolo;
	}

	public Protocolo getProtocoloHistorico() {
		return protocoloHistorico;
	}

	public void setProtocoloHistorico(Protocolo protocoloHistorico) {
		this.protocoloHistorico = protocoloHistorico;
	}

	public String getDataEnvioMensagem() {
		return dataEnvioMensagem;
	}

	public void setDataEnvioMensagem(String dataEnvioMensagem) {
		this.dataEnvioMensagem = dataEnvioMensagem;
	}

	public String getDataRecebidaMensagem() {
		return dataRecebidaMensagem;
	}

	public void setDataRecebidaMensagem(String dataRecebidaMensagem) {
		this.dataRecebidaMensagem = dataRecebidaMensagem;
	}

	public String getResponsavelMensagem() {
		return responsavelMensagem;
	}

	public void setResponsavelMensagem(String responsavelMensagem) {
		this.responsavelMensagem = responsavelMensagem;
	}

}

package Modelo;

public class FuncionarioOtimize {

	private int codigoFuncionario;
	private String nomeFuncionario;
	private boolean utilizarSkype;
	private String usuarioSkype;
	private String senhaSkype;
	private boolean presenteEmpresa;

	public int getCodigoFuncionario() {
		return codigoFuncionario;
	}

	public void setCodigoFuncionario(int codigoFuncionario) {
		this.codigoFuncionario = codigoFuncionario;
	}

	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	public boolean isUtilizarSkype() {
		return utilizarSkype;
	}

	public void setUtilizarSkype(boolean utilizarSkype) {
		this.utilizarSkype = utilizarSkype;
	}

	public String getUsuarioSkype() {
		return usuarioSkype;
	}

	public void setUsuarioSkype(String usuarioSkype) {
		this.usuarioSkype = usuarioSkype;
	}

	public String getSenhaSkype() {
		return senhaSkype;
	}

	public void setSenhaSkype(String senhaSkype) {
		this.senhaSkype = senhaSkype;
	}

	public boolean isPresenteEmpresa() {
		return presenteEmpresa;
	}

	public void setPresenteEmpresa(boolean presenteEmpresa) {
		this.presenteEmpresa = presenteEmpresa;
	}

	@Override
	public String toString() {
		return this.nomeFuncionario;

	}

}

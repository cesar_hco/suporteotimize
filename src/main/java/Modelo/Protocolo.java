package Modelo;

public class Protocolo {
	
	private int codigoProcolo;
	private String numeroProtocolo;
	private String clienteProtocolo;
	private String responsavelProtocolo;
	private String aberturaProtocolo;
	private String fechamentoProtocolo;
	private String situacaoProtocolo;

	public int getCodigoProcolo() {
		return codigoProcolo;
	}
	public void setCodigoProcolo(int codigoProcolo) {
		this.codigoProcolo = codigoProcolo;
	}
	public String getNumeroProtocolo() {
		return numeroProtocolo;
	}
	public void setNumeroProtocolo(String numeroProtocolo) {
		this.numeroProtocolo = numeroProtocolo;
	}
	public String getClienteProtocolo() {
		return clienteProtocolo;
	}
	public void setClienteProtocolo(String clienteProtocolo) {
		this.clienteProtocolo = clienteProtocolo;
	}
	public String getResponsavelProtocolo() {
		return responsavelProtocolo;
	}
	public void setResponsavelProtocolo(String responsavelProtocolo) {
		this.responsavelProtocolo = responsavelProtocolo;
	}
	public String getAberturaProtocolo() {
		return aberturaProtocolo;
	}
	public void setAberturaProtocolo(String aberturaProtocolo) {
		this.aberturaProtocolo = aberturaProtocolo;
	}
	public String getFechamentoProtocolo() {
		return fechamentoProtocolo;
	}
	public void setFechamentoProtocolo(String fechamentoProtocolo) {
		this.fechamentoProtocolo = fechamentoProtocolo;
	}
	public String getSituacaoProtocolo() {
		return situacaoProtocolo;
	}
	public void setSituacaoProtocolo(String situacaoProtocolo) {
		this.situacaoProtocolo = situacaoProtocolo;
	}
	
	
}

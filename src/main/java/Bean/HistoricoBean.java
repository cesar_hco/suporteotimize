package Bean;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Dao.HistoricoProtocoloDao;
import Modelo.HistoricoProtocolo;

@ManagedBean(name = "MBHistorico")
@SessionScoped
public class HistoricoBean {

	private ArrayList<HistoricoProtocolo> itensHistorico;
	private String valorProtocolo;

	public ArrayList<HistoricoProtocolo> getItensHistorico() {
		return itensHistorico;
	}

	public void setItensHistorico(ArrayList<HistoricoProtocolo> itensHistorico) {
		this.itensHistorico = itensHistorico;
	}

	public String getValorProtocolo() {
		return valorProtocolo;
	}

	public void setValorProtocolo(String valorProtocolo) {
		this.valorProtocolo = valorProtocolo;
	}

	public void consultarHistorico() {

		try {
		
			HistoricoProtocoloDao historicoProtocolo = new HistoricoProtocoloDao();

			itensHistorico = (ArrayList<HistoricoProtocolo>) historicoProtocolo.consultarHistorico(valorProtocolo);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}

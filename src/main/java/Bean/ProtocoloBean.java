package Bean;

import java.sql.SQLException;
import java.util.ArrayList;

import com.samczsun.skype4j.events.EventHandler;
import com.samczsun.skype4j.events.Listener;
import com.samczsun.skype4j.events.chat.message.MessageReceivedEvent;
import com.samczsun.skype4j.events.chat.message.MessageSentEvent;

import Dao.ProtocoloDao;
import Modelo.HistoricoProtocolo;
import Modelo.Protocolo;

public class ProtocoloBean implements Listener {

	private Protocolo protocolos;
	private ProtocoloDao protocoloDao = new ProtocoloDao();

	private ArrayList<Protocolo> itensProtocolos;

	public ArrayList<Protocolo> getItensProtocolos() {
		return itensProtocolos;
	}

	public void setItensProtocolos(ArrayList<Protocolo> itensProtocolos) {
		this.itensProtocolos = itensProtocolos;
	}

	boolean enviarmensagem = false;

	public Protocolo getProtocolos() {

		if (protocolos == null) {
			protocolos = new Protocolo();
		}

		return protocolos;
	}

	public void setProtocolos(Protocolo protocolos) {
		this.protocolos = protocolos;
	}

	int Protocolo = 0001;

	@EventHandler
	public void gerarProtocolo(MessageSentEvent e) {

		long time = (e.getMessage().getSentTime()) / 1000;

		DataUTC data = new DataUTC(time);
		String dataObtida = data.toString();

		try {

			String msgEnviada = e.getMessage().getContent().toString().toLowerCase();

			Protocolo protocoloInicinado = new Protocolo();

			if (e.getChat().getIdentity().toString().substring(0, 1).equals("8")) {
				protocoloInicinado.setClienteProtocolo(e.getChat().getIdentity().toString());
			}
			if (e.getChat().getIdentity().toString().substring(0, 2).equals("19")) {
				protocoloInicinado.setClienteProtocolo(e.getChat().getNomeGrupo().toString());
			}

			protocoloInicinado.setResponsavelProtocolo(e.getMessage().getClient().getUsername());
			protocoloInicinado.setAberturaProtocolo(dataObtida);
			protocoloInicinado.setSituacaoProtocolo("ABERTO");

			HistoricoProtocolo historicoProtocolo = new HistoricoProtocolo();

			historicoProtocolo.setDataRecebidaMensagem(dataObtida);
			historicoProtocolo.setMensagemRecebidaProtocolo(msgEnviada);
			historicoProtocolo.setResponsavelMensagem(e.getChat().getIdentity().toString());
			historicoProtocolo.setMensagemEnviadaProtocolo(e.getMessage().getSender().getId().toString());

			if (msgEnviada.length() >= 15) {

				if (msgEnviada.substring(0, 15).equals("já  finalizamos")) {

				} else {
					if (e.getChat().getIdentity().toString().substring(0, 1).equals("8")) {
						protocoloDao.ValidarExistenciaProtocolo(protocoloInicinado, false, true, historicoProtocolo);
					}
					if (e.getChat().getIdentity().toString().substring(0, 2).equals("19")) {
						protocoloDao.ValidarExistenciaProtocoloGrupo(protocoloInicinado, false, true,
								historicoProtocolo);
					}
				}

			} else {

				if (msgEnviada.startsWith("--")) {

					protocoloInicinado.setFechamentoProtocolo(dataObtida);

					if (e.getChat().getIdentity().toString().substring(0, 1).equals("8")) {
						protocoloDao.ValidarExistenciaProtocolo(protocoloInicinado, true, true, historicoProtocolo);
					}
					if (e.getChat().getIdentity().toString().substring(0, 2).equals("19")) {
						protocoloDao.ValidarExistenciaProtocoloGrupo(protocoloInicinado, true, true,
								historicoProtocolo);
					}

					if (protocoloDao.isSemProtocoloFinalizar() == false) {

						Mensagens.sendMensagem(e.getChat(), "já  finalizamos seu atendimento, anote o seu Protocolo: "
								+ protocoloDao.getNumeroFinalizadoProtocolo());
					}

				} else {

					if (e.getChat().getIdentity().toString().substring(0, 1).equals("8")) {
						protocoloDao.ValidarExistenciaProtocolo(protocoloInicinado, false, true, historicoProtocolo);
					}
					if (e.getChat().getIdentity().toString().substring(0, 2).equals("19")) {
						protocoloDao.ValidarExistenciaProtocoloGrupo(protocoloInicinado, false, true,
								historicoProtocolo);

					}

				}

			}

		} catch (SQLException erro) {

			erro.printStackTrace();
		}

	}

	@EventHandler
	public void onChat(MessageReceivedEvent e) {

		String msgRecebida = e.getMessage().getContent().toString().toLowerCase();

		long time = (e.getMessage().getSentTime()) / 1000;
		ProtocoloDao protocoloDao = new ProtocoloDao();

		DataUTC data = new DataUTC(time);
		String dataObtida = data.toString();
		Protocolo protocoloInicinado = new Protocolo();

		if (e.getChat().getIdentity().toString().substring(0, 1).equals("8")) {
			protocoloInicinado.setClienteProtocolo(e.getChat().getIdentity().toString());
		}
		if (e.getChat().getIdentity().toString().substring(0, 2).equals("19")) {
			protocoloInicinado.setClienteProtocolo(e.getChat().getNomeGrupo().toString());
		}

		protocoloInicinado.setNumeroProtocolo(
				dataObtida.substring(6, 10).concat(dataObtida.substring(3, 5)).concat(dataObtida.substring(0, 2)));
		protocoloInicinado.setResponsavelProtocolo(e.getMessage().getClient().getUsername());
		protocoloInicinado.setAberturaProtocolo(dataObtida);
		protocoloInicinado.setSituacaoProtocolo("ABERTO");

		HistoricoProtocolo historicoProtocolo = new HistoricoProtocolo();

		historicoProtocolo.setDataRecebidaMensagem(dataObtida);
		historicoProtocolo.setMensagemRecebidaProtocolo(msgRecebida);
		historicoProtocolo.setResponsavelMensagem(e.getMessage().getClient().getUsername());
		historicoProtocolo.setMensagemEnviadaProtocolo(e.getMessage().getSender().getId().toString());

		if (msgRecebida.length() >= 15) {

			if (msgRecebida.substring(0, 15).equals("já  finalizamos")) {

			} else {
				if (e.getChat().getIdentity().toString().substring(0, 1).equals("8")) {
					try {
						protocoloDao.ValidarExistenciaProtocolo(protocoloInicinado, false, false, historicoProtocolo);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (e.getChat().getIdentity().toString().substring(0, 2).equals("19")) {
					try {
						protocoloDao.ValidarExistenciaProtocoloGrupo(protocoloInicinado, false, false,
								historicoProtocolo);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}

		} else {

			if (e.getChat().getIdentity().toString().substring(0, 1).equals("8")) {
				try {
					if (msgRecebida.startsWith("--")) {
					}
					if (msgRecebida.startsWith("ok")) {

					}
					if (msgRecebida.startsWith("obrigado")) {

					}
					if (msgRecebida.startsWith("valeu")) {

					}
					if (msgRecebida.startsWith("certo")) {

					}

					else {
					
					
					protocoloDao.ValidarExistenciaProtocolo(protocoloInicinado, false, false, historicoProtocolo);
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			if (e.getChat().getIdentity().toString().substring(0, 2).equals("19")) {
				try {
					if (msgRecebida.startsWith("--")) {
					}
					if (msgRecebida.startsWith("ok")) {

					}
					if (msgRecebida.startsWith("obrigado")) {

					}
					if (msgRecebida.startsWith("valeu")) {

					}
					if (msgRecebida.startsWith("certo")) {

					}

					else {

						protocoloDao.ValidarExistenciaProtocoloGrupo(protocoloInicinado, false, false,
								historicoProtocolo);
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			if (protocoloDao.isAberturaProtocolo() == true) {

				if (e.getChat().getIdentity().toString().substring(0, 1).equals("8")) {
					Mensagens.sendMensagem(e.getChat(), "Ola, em que podemos te ajudar? ");
				}

			}
		}

	}
}

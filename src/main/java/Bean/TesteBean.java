package Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import Dao.ProtocoloDao;
import Modelo.Protocolo;

@ManagedBean(name = "MBProtocolo")
@SessionScoped
public class TesteBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Protocolo> itensProtocolosAbertos;
	private ArrayList<Protocolo> itensProtocolosFechados;
	private ArrayList<Protocolo> itensProtocolosConsultados;
	private String tipoconsulta;
	private String situacao;
	private String consulta;
	private String valorInformado;
	private String valorConsulta;
	private String dataInicio;
	private String dataFim;

	public String getValorConsulta() {
		return valorConsulta;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public void setValorConsulta(String valorConsulta) {
		this.valorConsulta = valorConsulta;
	}

	public ArrayList<Protocolo> getItensProtocolosAbertos() {
		return itensProtocolosAbertos;
	}

	public void setItensProtocolosAbertos(ArrayList<Protocolo> itensProtocolosAbertos) {
		this.itensProtocolosAbertos = itensProtocolosAbertos;
	}

	public ArrayList<Protocolo> getItensProtocolosFechados() {
		return itensProtocolosFechados;
	}

	public void setItensProtocolosFechados(ArrayList<Protocolo> itensProtocolosFechados) {
		this.itensProtocolosFechados = itensProtocolosFechados;
	}

	public ArrayList<Protocolo> getItensProtocolosConsultados() {
		return itensProtocolosConsultados;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public String getValorInformado() {
		return valorInformado;
	}

	public void setValorInformado(String valorInformado) {
		this.valorInformado = valorInformado;
	}

	public void setItensProtocolosConsultados(ArrayList<Protocolo> itensProtocolosConsultados) {
		this.itensProtocolosConsultados = itensProtocolosConsultados;
	}

	

	
	

	public List getTipoconsulta() {
		List itens = new ArrayList(0);
		itens.add(new SelectItem("protocolo", "Protocolo"));
		itens.add(new SelectItem("responsavel", "Responsavel Otimize"));
		itens.add(new SelectItem("cliente", "Cliente"));

		return itens;
	}

	public void setTipoconsulta(String tipoconsulta) {
		this.tipoconsulta = tipoconsulta;
	}

	public List getsituacao() {
		List itens = new ArrayList(0);
		itens.add(new SelectItem("AMBOS", "Ambos"));
		itens.add(new SelectItem("ABERTO", "Aberto"));
		itens.add(new SelectItem("FINALIZADO", "Finalizado"));

		return itens;
	}

	public void setsituacao(String situacao) {
		this.situacao = situacao;
	}

	@PostConstruct
	public void ListarProtocolos() {

		try {

			ProtocoloDao protocoloDao = new ProtocoloDao();
			itensProtocolosAbertos = (ArrayList<Protocolo>) protocoloDao.ListarProtocoloAberto();
			itensProtocolosFechados = (ArrayList<Protocolo>) protocoloDao.ListarProtocoloFechado();
			// FacesContext.getCurrentInstance().getExternalContext().redirect("PainelGestor.xhtml");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void AtualizarTela() {

		final long TEMPO = (1000 * 20);

		Timer timer = null;
		if (timer == null) {
			timer = new Timer();
			TimerTask tarefa = new TimerTask() {
				public void run() {
					try {
						ListarProtocolos();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			timer.scheduleAtFixedRate(tarefa, TEMPO, TEMPO);
		}
	}

	public void consultarProtocolos() {

		try {

		

				ProtocoloDao protocoloDao = new ProtocoloDao();

				itensProtocolosConsultados = (ArrayList<Protocolo>) protocoloDao.ListarProtocolos(valorConsulta ,dataInicio , dataFim , consulta , valorInformado);
			
				} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}

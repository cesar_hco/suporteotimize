package Bean;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import Dao.FuncionarioOtimizeDao;
import Dao.LigacoesDao;
import Dao.UnidadeDao;
import Modelo.FuncionarioOtimize;
import Modelo.Ligacoes;
import Modelo.UnidadeOrganizacional;

@ManagedBean(name = "MBLigacoes")
public class LigacoesBean {

	private LigacoesDao ligacao;
	private FuncionarioOtimize funcionario;
	private UnidadeOrganizacional unidade;
	private Ligacoes ligacoes;
	private String unidadeEnsino;

	private ArrayList<Ligacoes> itensLigacoes;
	private ArrayList<FuncionarioOtimize> funcionarioOtimize;
	private ArrayList<UnidadeOrganizacional> unidadeOrganizacional;

	public Ligacoes getLigacoes() {
		return ligacoes;
	}

	public void setLigacoes(Ligacoes ligacoes) {
		this.ligacoes = ligacoes;
	}

	public LigacoesDao getLigacao() {
		return ligacao;
	}

	public void setLigacao(LigacoesDao ligacao) {
		this.ligacao = ligacao;
	}

	public ArrayList<Ligacoes> getItensLigacoes() {
		return itensLigacoes;
	}

	public void setItensLigacoes(ArrayList<Ligacoes> itensLigacoes) {
		this.itensLigacoes = itensLigacoes;
	}

	public ArrayList<FuncionarioOtimize> getFuncionarioOtimize() {
		return funcionarioOtimize;
	}

	public void setFuncionarioOtimize(ArrayList<FuncionarioOtimize> funcionarioOtimize) {
		this.funcionarioOtimize = funcionarioOtimize;
	}

	public FuncionarioOtimize getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(FuncionarioOtimize funcionario) {
		this.funcionario = funcionario;
	}

	public ArrayList<UnidadeOrganizacional> getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}

	public void setUnidadeOrganizacional(ArrayList<UnidadeOrganizacional> unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	public UnidadeOrganizacional getUnidade() {
		return unidade;
	}

	public void setUnidade(UnidadeOrganizacional unidade) {
		this.unidade = unidade;
	}

	
	
	public String getUnidadeEnsino() {
		return unidadeEnsino;
	}

	public void setUnidadeEnsino(String unidadeEnsino) {
		this.unidadeEnsino = unidadeEnsino;
	}

	@PostConstruct
	public void Listar() {

		try {

			FuncionarioOtimizeDao funcionariodao = new FuncionarioOtimizeDao();
			UnidadeDao unidadeDao = new UnidadeDao();
			funcionarioOtimize = (ArrayList<FuncionarioOtimize>) funcionariodao.funcionarioOtimize();
			unidadeOrganizacional = (ArrayList<UnidadeOrganizacional>) unidadeDao.unidadeOrganizacional();

		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	public void salvar() {

		try {
			
			System.out.println("FOI");

			LigacoesDao Ligacoesdao = new LigacoesDao();
			Ligacoesdao.GravarLigacoes(ligacoes);
			FacesMessage msg = new FacesMessage("Cadastro Realizado com Sucesso !");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} catch (Exception e) {
			FacesMessage msg = new FacesMessage("Entrar em contato com o Tercio!");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			e.printStackTrace();

		}

	}

	public String prepararSalvar() {

		ligacoes = new Ligacoes();
		
		return "LigacoesAtendidas.xhtml?faces-redirect=true";

	}

}
